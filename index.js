const express = require("express");
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.listen(port, () => {
    console.log(`Server running at port ${port}`);
});

const mongoose = require("mongoose");
mongoose.connect(
    "mongodb+srv://admin:admin@zuittbatch243.fvxpkh7.mongodb.net/?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Successfully connected to the database"));

/* Schema */
const userSchema = new mongoose.Schema({
    username: String,
    password: String,
});

/* Model */
const User = mongoose.model("User", userSchema);

/* Post Route */
app.post("/signup", (req, res) => {
    User.findOne({ username: req.body.username }, (err, result) => {
        if (result != null && result.username == req.body.username) {
            let nameStr = req.body.username;
            return res.send(
                `Username "${nameStr}" is already registered.`
            );
        } else {
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            });

            newUser.save((saveErr, saveUser) => {
                if (saveErr) {
                    return console.error(saveErr);
                } else {
                    return res.status(200).send(`New user registered`);
                }
            });
        }
    });
});